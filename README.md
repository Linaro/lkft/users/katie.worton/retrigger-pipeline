# Retrigger pipeline

This repository holds the code for retriggering TuxSuite tests and builds alongside the Gitlab CI pipeline used to run the retrigger process.

## Retriggering using the pipeline

To retrigger a build or test from a Gitlab CI pipeline, set either the `BUILD_TESTRUN_ID` or `TEST_TESTRUN_ID` parameter in the Gitlab CI Run Pipeline dialogue in this project. The default location for the tests to be run and stored is specified in `base.yml`, alongside other defaults such as rerun count and reproducer file names.

### Retriggering builds

To retrigger a build using the pipeline, when launching the pipeline, set the environment variable `BUILD_TESTRUN_ID` to the SQUAD TestRun ID of the desired build to rebuild.

### Retriggering tests
To retrigger a test using the pipeline, when launching the pipeline, set the environment variable `TEST_TESTRUN_ID` to the SQUAD TestRun ID of the desired test to rerun.

## Retriggering using the scripts locally

To retrigger a build or test locally:

1. **Download and install prerequisites:** Call `./retrigger_utils/download_prerequisites.py` to download and install prerequisite files and Python packages. The default values for required environment variables are loaded from `base.yml`. If any of the required environment variables are missing, an error will be raised stating which environment variables are missing.
2. **Setup for reproducer download:** Set either `BUILD_TESTRUN_ID` or `TEST_TESTRUN_ID` as an environment variable. These should contain the SQUAD TestRun ID associated with the given test or build. If both environment variables are provided, both the build and test will be fetched. If a parameter `REBUILD` is set to "true" and `BUILD_TESTRUN_ID` is not provided, the build reproducer for the provided `TEST_TESTRUN_ID` will be fetched. The file names to save the build reproducer to is specified by `BUILD_REPRODUCER_FILE` and the file name to save the test reproducer is `TEST_REPRODUCER_FILE` - defaults for these are specified in `base.yml`
3. **Fetch reproducer:** Call `./retrigger_utils/fetch_reproducer.py` to fetch the reproducer(s) for the specified build or test.
4. **Create plan:** Call `./retrigger_utils/create_plan.py` to create a reproducer plan file for the fetched build or test. If `BUILD_TESTRUN_ID` is provided a reproducer file will be created for the build fetched in the previous stage. If `TEST_TESTRUN_ID` is provided, a plan reproducer will be created from the test reproducer fetched in the previous stage. If both `BUILD_TESTRUN_ID` and `TEST_TESTRUN_ID` are provided then a reproducer plan will be created that rebuilds the provided build then reruns the provided test on the provided build. The reproducer file names are passed in as inputs via the environment variables `BUILD_REPRODUCER_FILE` and `TEST_REPRODUCER_FILE`. To specify how many times to rerun a test, set the `RUN_COUNT` parameter to the desired number. The generated plan file will be written to the file name specified by `OUTPUT_REPRODUCER_FILE`.
5. **Launch reproducer:** Call `./retrigger_utils/launch_reproducer.py` to launch the generated reproducer plan specified by environment variable `OUTPUT_REPRODUCER_FILE`. The results will be stored on the provided `QA_SERVER` in the specified `QA_TEAM`, `QA_PROJECT` and `QA_PROJECT_NAME`, in the provided `BUILD_NAME`.

## Required environment variables for retriggering

The following environment variables are required to retrigger and must be provided:
- `TEST_TESTRUN_ID` or `BUILD_TESTRUN_ID` - the SQUAD TestRun ID of the build or test to retrigger.
- `SQUAD_BUILD_NAME` - the name for the SQUAD build for storing the retrigger results


The following environment variables are required and have default values defined in `base.yml`:
- `BUILD_REPRODUCER_FILE` - name for build reproducer
- `TEST_REPRODUCER_FILE` - name for test reproducer
- `OUTPUT_REPRODUCER_FILE` - output reproducer file name
- `JSON_OUT_NAME` - name for the json out file when submitting the tuxsuite plan
- `RUN_COUNT` - number of times test reproducers should be rerun
- `REBUILD` Whether to fetch the build for a given test - set to "true" to fetch the test's build
- `QA_SERVER` - Address for SQUAD instance
- `QA_TEAM` - QA\_TEAM for storing retriggered build/test results
- `QA_PROJECT` - QA\_PROJECT for storing retriggered build/test results
- `QA_PROJECT_NAME` - QA\_PROJECT\_NAME for storing retriggered build/test results
- `COMMON_BASE_URL` - URL for common repo for downloading required scripts
- `SQUAD_CLIENT_UTILS_BASE_URL` - Location of squad-client-utils repo for downloading required scripts
- `QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT` - required for the create-or-update function from the common repo
-  `QA_NOTIFICATION_TIMEOUT` - required for the create-or-update function from the common repo
-  `QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT` - required for the create-or-update function from the common repo

The following environment variables are required and should be set in the Gitlab CI pipeline variables if running in the pipeline:
- `QA_REPORTS_TOKEN` - token to allow submission to results to SQUAD
- `TUXSUITE_TOKEN` - token to allow submission of job to TuxSuite
- `TUXSUITE_PUBLIC_KEY` - public key for callback from TuxSuite to SQUAD

## Limitations and future work

Currently, the pipeline should be launched from Gitlab through the GUI, but in future there will be scripting provided as a wrapper for launching the Gitlab CI pipeline from the command line. The scripts can also be launched locally.

Currently, the is not the functionality to run a single test from within a suite, but this may be investigated in the future.

Currently `RUN_COUNT` is restricted to 10 to limit number of reruns during testing.

## License

[MIT](https://gitlab.com/Linaro/lkft/mirrors/retrigger-pipeline/-/blob/main/LICENSE)

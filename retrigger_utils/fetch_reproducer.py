#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import os
import sys
import yaml

sys.path.append(".")
from retrigger_utils import (  # noqa
    check_fetch_reproducer,
    download_file,
    install_prerequisites,
    log_proc,
    logger,
    Settings,
    run_cmd,
)


#
#   Required environment variables
#
required_vars = [
]


def fetch_reproducer(testrun_id, filename):
    print("Files in directory", os.listdir())
    cmd = [
        "python",
        "squad-create-reproducer-from-testrun",
        "--testrun",
        str(testrun_id),
        "--filename",
        filename,
    ]
    proc = run_cmd(cmd=cmd)

    log_proc(proc)

    return proc.ok


def get_build_reproducer_from_testrun_reproducer(settings):
    with open(settings.TEST_REPRODUCER_FILE, "r") as f:
        plan = yaml.safe_load(f)

    kernel_url = plan["jobs"][0]["test"]["kernel"]

    basename_kernel_url = os.path.dirname(kernel_url)

    return download_file(
        basename_kernel_url + "/tux_plan.yaml", settings.BUILD_REPRODUCER_FILE
    )


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    check_fetch_reproducer_ok = check_fetch_reproducer(settings)
    if not check_fetch_reproducer_ok:
        return False

    install_prerequisites_ok = install_prerequisites(settings)
    if not install_prerequisites_ok:
        return False

    # Fetch test reproducer
    if settings.TEST_TESTRUN_ID:
        fetch_test_reproducer_ok = fetch_reproducer(
            testrun_id=settings.TEST_TESTRUN_ID, filename=settings.TEST_REPRODUCER_FILE
        )
        if not fetch_test_reproducer_ok:
            return False

    # Fetch build reproducer
    if settings.BUILD_TESTRUN_ID:
        fetch_build_reproducer_ok = fetch_reproducer(
            testrun_id=settings.BUILD_TESTRUN_ID,
            filename=settings.BUILD_REPRODUCER_FILE,
        )
    elif settings.REBUILD == "true":
        fetch_build_reproducer_ok = get_build_reproducer_from_testrun_reproducer(
            settings=settings
        )
    else:
        # If we aren't fetching a build, the check should pass
        fetch_build_reproducer_ok = True

    if not fetch_build_reproducer_ok:
        return False

    return True


if __name__ == "__main__":
    sys.exit(0 if main() else 1)

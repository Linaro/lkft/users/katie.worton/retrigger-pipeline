#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import sys

sys.path.append(".")
from retrigger_utils import (  # noqa
    check_create_plan,
    log_proc,
    logger,
    install_prerequisites,
    Settings,
    run_cmd,
)


#
#   Required environment variables
#
required_vars = [
    "OUTPUT_REPRODUCER_FILE",
]


def create_plan(settings):
    tuxsuite_cmd = [
        "tuxsuite",
        "plan",
        "create",
        "--output-plan",
        settings.OUTPUT_REPRODUCER_FILE,
    ]

    if settings.BUILD_TESTRUN_ID:
        build_parameters = [
            "--build-plan",
            settings.BUILD_REPRODUCER_FILE,
        ]
        tuxsuite_cmd.extend(build_parameters)

    if settings.TEST_TESTRUN_ID:
        test_parameters = [
            "--test-plan",
            settings.TEST_REPRODUCER_FILE,
            "--test-retrigger",
            settings.RUN_COUNT,
        ]
        tuxsuite_cmd.extend(test_parameters)

    proc = run_cmd(cmd=tuxsuite_cmd)
    log_proc(proc)

    return proc.ok


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    check_create_plan_ok = check_create_plan(settings)
    if not check_create_plan_ok:
        return False

    install_prerequisites_ok = install_prerequisites(settings)
    if not install_prerequisites_ok:
        return False

    # Create the plan
    create_plan_ok = create_plan(settings=settings)

    return create_plan_ok


if __name__ == "__main__":
    sys.exit(0 if main() else 1)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT


import os
import sys

sys.path.append(".")
from retrigger_utils import (  # noqa
    check_all_pipeline_stages,
    download_file,
    log_proc,
    logger,
    run_cmd,
    Settings,
)


def download_prerequisite_scripts(settings):
    """
    Download the required scripts for retriggering, create or update SQUAD project
    """
    download_file(settings.SQUAD_CLIENT_UTILS_BASE_URL + "/squadutilslib.py")
    download_file(
        settings.SQUAD_CLIENT_UTILS_BASE_URL + "/squad-create-reproducer-from-testrun"
    )
    download_file(settings.COMMON_BASE_URL + "/register-callback.sh")
    download_file(settings.COMMON_BASE_URL + "/squad-client-cup.sh")
    create_or_update = run_cmd(
        ["bash", "squad-client-cup.sh", settings.QA_PROJECT, settings.QA_PROJECT_NAME]
    )
    log_proc(create_or_update)
    if not create_or_update.ok:
        return False

    download_file(
        settings.SQUAD_CLIENT_UTILS_BASE_URL + "/requirements.txt",
        "requirements_squad_client_utils.txt",
    )
    pip_install = run_cmd(["pip", "install", "-r", "requirements.txt"])
    log_proc(pip_install)
    if not pip_install.ok:
        return False

    pip_upgrade_tuxsuite = run_cmd(["pip", "install", "--upgrade", "tuxsuite"])
    log_proc(pip_upgrade_tuxsuite)

    if not pip_upgrade_tuxsuite.ok:
        return False

    logger.info(f"Files in directory {os.listdir()}")

    return True


#
#   Required environment variables for pipeline
#
required_vars = [
    "SQUAD_BUILD_NAME",
    "BUILD_REPRODUCER_FILE",
    "TEST_REPRODUCER_FILE",
    "OUTPUT_REPRODUCER_FILE",
    "JSON_OUT_NAME",
    "RUN_COUNT",
    "REBUILD",
    "QA_SERVER",
    "QA_TEAM",
    "QA_PROJECT",
    "QA_PROJECT_NAME",
    "COMMON_BASE_URL",
    "SQUAD_CLIENT_UTILS_BASE_URL",
    "QA_WAIT_BEFORE_NOTIFICATION_TIMEOUT",
    "QA_NOTIFICATION_TIMEOUT",
    "QA_FORCE_FINISHING_BUILDS_ON_TIMEOUT",
    "QA_REPORTS_TOKEN",
    "TUXSUITE_TOKEN",
    "TUXSUITE_PUBLIC_KEY",
]


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False

    # Perform prerequisite checks for all scripts
    check_all_pipeline_stages_ok = check_all_pipeline_stages(settings)
    if not check_all_pipeline_stages_ok:
        return False

    # Download prerequisites for scripts
    download_prerequisites_ok = download_prerequisite_scripts(settings=settings)
    if not download_prerequisites_ok:
        return False

    return True


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
